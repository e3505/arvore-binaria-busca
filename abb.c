#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
  int info;
  struct arvore *esq;
  struct arvore *dir;
} Arvore;

static Arvore * anterior = NULL; 

int eh_abb(Arvore * arvore)
{
    if (arvore)
    {
        if (!eh_abb(arvore->esq))
            return 0;
        if (anterior != NULL && arvore->info <= anterior->info)
            return 0;
        anterior = arvore;
        return eh_abb(arvore->dir);
    }
    return 1;
}

Arvore* inserir (Arvore *a, int v) {
  if (a == NULL) {
    a = (Arvore*)malloc(sizeof(Arvore));
    a->info = v;
    a->esq = a->dir = NULL;
  }
  else if (v < a->info) {
    a->esq = inserir (a->esq, v);
  }
  else { a->dir = inserir (a->dir, v); }
  return a;
}

void print(Arvore * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

int main(){
    Arvore * a;

    a = inserir(NULL,50);
    a = inserir(a,30);
    a = inserir(a,90);
    a = inserir(a,20);
    a = inserir(a,40);
    a = inserir(a,95);
    a = inserir(a,10);
    a = inserir(a,35);
    a = inserir(a,45);
    a = inserir(a,37);
      
    if (eh_abb(a))
        printf("Árvore é uma ABB\n");
    else
        printf("Árvore não é uma ABB\n");
    return 0;

}
